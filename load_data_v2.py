import os
import pandas as pd
import pymongo
import datetime

homicide_dir = 'data/database.csv'
uscitites_dir = 'data/sub-est2019_all.csv'

#mongo conection
client = pymongo.MongoClient()
db = client['HD_Database_v1']
homicide_collection = db['homicide_v2']


uscitites = pd.read_csv(uscitites_dir)
#print(uscitites)
city_pop = dict()
for index, row in uscitites.iterrows():
    typ = row['NAME'][-4:]
    #print(row['CENSUS2010POP'])
    if row['CENSUS2010POP'] == 'A':
        continue
    if typ[0] == 'c' or typ[0] == 't':
        element = (row['NAME'][:-5],row['STNAME'])
        city_pop[element] = int(row['CENSUS2010POP'])
    elif typ[0] == 'l':
        #muni cipa lity
        element = (row['NAME'][:-13],row['STNAME'])
        city_pop[element] = int(row['CENSUS2010POP'])
    elif typ[0] == 'o':
        #bor ough
        element = (row['NAME'][:-8],row['STNAME'])
        city_pop[element] = int(row['CENSUS2010POP'])
    elif typ[0] == 'u':
        #Co unty
        element = (row['NAME'][:-7],row['STNAME'])
        city_pop[element] = int(row['CENSUS2010POP'])


#ke = ('Anchorage', 'Alaska')
#print(ke in city_pop)
#print(pds)
group_dic = dict()
write_to_db = list()
not_found =set()
data = pd.read_csv(homicide_dir)
for index, row in data.iterrows():
    pop_key = (row['City'],row['State'])
    population = 1
    if pop_key in city_pop:
        population = city_pop[pop_key]
    else:
        not_found.add(pop_key)
    
    write_to_db.append({
        'year': row['Year'],
        'Month': row['Month'],
        'agency_code': row['Agency Code'],
        'agency_name': row['Agency Name'],
        'agency_type': row['Agency Type'],
        'city': row['City'],
        'state': row['State'],
        'total_population': population,
        'incidents': {
            'crime_type': row['Crime Type'],
            'solved': row['Crime Solved'],
            'victim_sex': row['Victim Sex'],
            'victim_age': row['Victim Age'],
            'victim_race': row['Victim Race'],
            'victim_ethnicity': row['Victim Ethnicity'],
            'perpetrator_sex': row['Perpetrator Sex'],
            'perpetrator_age': row['Perpetrator Age'],
            'perpetrator_race': row['Perpetrator Race'],
            'perpetrator_ethnicity': row['Perpetrator Ethnicity'],
            'relationship': row['Relationship'],
            'weapon': row['Weapon'],
            'victim_count': row['Victim Count'],
            'perpetrator_count': row['Perpetrator Count'],
            'record_source': row['Record Source']
        }
        })


print("Total cities not found:",len(not_found))

#insert into database
homicide_collection.insert_many(write_to_db)
