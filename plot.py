import numpy as np
import matplotlib.pyplot as plt
data = [
    [0.743, 1.250, 1.34, 1.58, 1.24, 1.27, 1.38, 1.56, 1.25, 0.23],
    [1.30, 0.60, 1.05, 1.73, 0.67, 0.63, 1.18, 1.34, 0.576, 0.529],
    [1.30, 0.125, 1.05, 1.73, 0.314, 0.156, 1.18, 1.34, 0.49, 0.38]
    ]
X = np.arange(10)
fig, ax = plt.subplots()
v1_bar = ax.bar(X       , data[0], color = 'b', width = 0.25)
v2_bar = ax.bar(X + 0.25, data[1], color = 'g', width = 0.25)
v3_bar = ax.bar(X + 0.50, data[2], color = 'r', width = 0.25)

ax.set_ylabel('Seconds')
ax.set_xlabel('Questions')
ax.set_xticks(X + 0.50 / 2)
ax.set_xticklabels(('1-U', '2-U', '3-U', '4-U', '5-Z', '6-U', '7-Z', '8-Z', '9-Z', '10-Z'))

ax.legend((v1_bar[0], v2_bar[0], v3_bar[0]), ('v1', 'v2', 'v3'))
plt.show()