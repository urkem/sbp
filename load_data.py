import os
import pandas as pd
import pymongo
import datetime

homicide_dir = 'data/database.csv'
uscitites_dir = 'data/sub-est2019_all.csv'

#mongo conection
client = pymongo.MongoClient()
db = client['HD_Database_v1']
homicide_collection = db['homicide']


uscitites = pd.read_csv(uscitites_dir)
city_pop = dict()
for index, row in uscitites.iterrows():
    typ = row['NAME'][-4:]
    if row['CENSUS2010POP'] == 'A':
        continue

    if typ[0] == 'c' or typ[0] == 't':
        element = (row['NAME'][:-5],row['STNAME'])
        city_pop[element] = int(row['CENSUS2010POP'])
    elif typ[0] == 'l':
        #muni cipa lity
        element = (row['NAME'][:-13],row['STNAME'])
        city_pop[element] = int(row['CENSUS2010POP'])
    elif typ[0] == 'o':
        #bor ough
        element = (row['NAME'][:-8],row['STNAME'])
        city_pop[element] = int(row['CENSUS2010POP'])
    elif typ[0] == 'u':
        #Co unty
        element = (row['NAME'][:-7],row['STNAME'])
        city_pop[element] = int(row['CENSUS2010POP'])


group_dic = dict()
not_found =set()
data = pd.read_csv(homicide_dir)
for index, row in data.iterrows():
    pop_key = (row['City'],row['State'])
    population = 1
    if pop_key in city_pop:
        population = city_pop[pop_key]
    else:
        not_found.add(pop_key)
    
    element = (row['Year'],row['Month'],row['Agency Code'],row['Agency Name'],row['Agency Type'],row['City'],row['State'], population)
    if element in group_dic:
        group_dic[element].append({
            'crime_type': row['Crime Type'],
            'solved': row['Crime Solved'],
            'victim_sex': row['Victim Sex'],
            'victim_age': row['Victim Age'],
            'victim_race': row['Victim Race'],
            'victim_ethnicity': row['Victim Ethnicity'],
            'perpetrator_sex': row['Perpetrator Sex'],
            'perpetrator_age': row['Perpetrator Age'],
            'perpetrator_race': row['Perpetrator Race'],
            'perpetrator_ethnicity': row['Perpetrator Ethnicity'],
            'relationship': row['Relationship'],
            'weapon': row['Weapon'],
            'victim_count': row['Victim Count'],
            'perpetrator_count': row['Perpetrator Count'],
            'record_source': row['Record Source']
        })
    else:
        group_dic[element]= [{
            'crime_type': row['Crime Type'],
            'solved': row['Crime Solved'],
            'victim_sex': row['Victim Sex'],
            'victim_age': row['Victim Age'],
            'victim_race': row['Victim Race'],
            'victim_ethnicity': row['Victim Ethnicity'],
            'perpetrator_sex': row['Perpetrator Sex'],
            'perpetrator_age': row['Perpetrator Age'],
            'perpetrator_race': row['Perpetrator Race'],
            'perpetrator_ethnicity': row['Perpetrator Ethnicity'],
            'relationship': row['Relationship'],
            'weapon': row['Weapon'],
            'victim_count': row['Victim Count'],
            'perpetrator_count': row['Perpetrator Count'],
            'record_source': row['Record Source']
        }]

write_to_db = list()
print("Total not found:",len(not_found))
for el in group_dic.keys():
    #(row['Year'],row['Month'],row['Agency Code'],row['Agency Name'],row['Agency Type'],row['City'],row['State'], population)
    write_to_db.append({
        'year': el[0],
        'Month': el[1],
        'agency_code': el[2],
        'agency_name': el[3],
        'agency_type': el[4],
        'city': el[5],
        'state': el[6],
        'total_population': el[7],
        'total_incidents': len(group_dic[el]),
        'incidents': group_dic[el]
        })


#insert into database
homicide_collection.insert_many(write_to_db)
